-module(fizz_buzz).
-export([fizzbuzz/1]).
%% Реализовать Fizz Buzz
%% https://habr.com/ru/post/298134/

fizzbuzz(N) -> fizzbuzz(N, []).

fizzbuzz(0, Acc) -> Acc;
fizzbuzz(N, Acc) ->
    IsFizz = N rem 3 =:= 0,
    IsBuzz = N rem 5 =:= 0,
    fizzbuzz(N-1, 
        if
            IsFizz andalso IsBuzz -> ["FizzBuzz" | Acc];
            IsFizz -> ["Fizz" | Acc];
            IsBuzz -> ["Buzz" | Acc];
            true -> [N | Acc]
        end).
